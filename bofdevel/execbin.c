#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <linux/sched.h>

int main(int argc, char **argv)
{
 // enough arguments? if not print help, otherwise print progress as we go...
 if(argc > 1) printf("[WORKING] opening file '%s'...", argv[1]);
 else {       printf("usage: execbin <file>       <-- where <file> is shellcode that gets loaded & executed.\n");
              exit(1); }

 // open file, if error: abort as usual...
 FILE *fp = fopen(argv[1], "rb");
 if(argv[1][0] == '-' && argv[1][1] == 0 && fp == 0) { printf("\r[<STDIN>]\n"); }
 else if(fp) {                                         printf("\r[OPENED ]\n"); }
 else {                                                printf("\r[FAILED!]\n");
                                                       exit(1); }

 // next step: allocate memory that is executable...
 printf("[WORKING] allocating sufficient amount of executable memory with mmap()...");

 // get size of file...
 size_t psc_sz;

 if(fp) { fseek(fp, 0, SEEK_END);
          psc_sz = ftell(fp);
          fseek(fp, 0, SEEK_SET); }
 else {   psc_sz = 1024*1024; /* 1Mb should suffice in case '-' stdin-input were is used */ }

 // try to allocate that amount of memory
 unsigned char *psc = (unsigned char*)mmap(0, psc_sz,
                                           PROT_EXEC|PROT_READ|PROT_WRITE,
                                           MAP_ANONYMOUS|MAP_SHARED, 0, 0);

 // abort if any error...
 if(psc == MAP_FAILED) { printf("\r[FAILED!]\n");
                         fclose(fp);
                         exit(1); }
 else {                  printf("\r[MAPPED ]\n"); }

 // read contents of file, or stdin until EOF or error
 printf("[%s] reading %s...", fp == 0 ? "<STDIN>" : "INFILE ", fp == 0 ? "input, of up to (at most) %d bytes" : "contents of file");

 if(fp) {
  fread(psc, psc_sz, 1, fp);
  fclose(fp);
  printf("\r[**EOF**]\n");
 } else {
  unsigned int i, read_bytes = 0;
  read_again:
  i = read(0, &psc[read_bytes], psc_sz - read_bytes);
  if(i > 0) {
   read_bytes += i;
   if(read_bytes < psc_sz)
    goto read_again;
   else
   printf("\r[**EOF**]\n");
  } else {
   printf("\r[FAILED!]\n");
  }
 }

 // execute...
 printf("[!!!!!!!] executing loaded code...\n");
 void (*pfunc)(void);// = psc;
 pfunc = psc;
 pfunc();

 printf("[SUCCESS] returned safely from shellcode!\n");

 // munmap() memory
 munmap(psc, psc_sz);

 exit(0);
}
