#!/bin/bash

if [[ "$1" == "" || "$2" == "" ]] ; then
 echo "usage: $0 <tempfile.asm> <tempfile.bin>";
 exit 1;
fi

cat >"$1";

nasm -f bin "$1" -o "$2" 1>&2;

cat "$2"
