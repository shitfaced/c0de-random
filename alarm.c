#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dlfcn.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <signal.h>

char *pprogname;
char **pargv;
int *pargc;

void sigalrm(int signum) {
 printf(" signal caught!\n");

 printf("preparing alarm() and signal()...\n");
 signal(SIGALRM, sigalrm);
 alarm(5);
 printf(" done!\n");

 printf("executing program %s thru execve...\n", pprogname);
 //char *args[] = {pprogname, 0};
 char *args[512] = {0};
 args[0] = pprogname;
 int i;
 for(i=0;i<(*pargc)-2;i++) {
  args[1+i] = pargv[i+2];
 }
 char *envp[] = {0, 0};
 execve(args[0], args, envp);
 printf(" done!\n");

 exit(0);
 //return;
}

int main(int argc, char **argv) {
 pargv = argv;
 pargc = &argc;

 int i;
 pid_t pid, ppid;

 printf("setting pprogname to argv[1]...\n");
 pprogname = argv[1];
 printf(" done!\n");

 printf("preparing alarm() and signal()...\n");
 signal(SIGALRM, sigalrm);
 alarm(5);
 printf(" done!\n");

/*
 printf("executing program %s thru execve...", argv[1]);
 char *args[] = {argv[1], 0};
 char *envp[] = {0, 0};
 execve(args[0], args, envp);
 printf(" done!\n");
*/

 printf("executing pause()...\n");
 //for(;;){}
 pause();
 printf(" done.\n");

 exit(0);
}
